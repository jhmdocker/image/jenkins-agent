#!/bin/bash -e
dockerd --log-level warn &

R=1
while [[ "$R" != "0" ]]
do
  echo Esperando docker...
  sleep 1
  docker version 2>&1 > /dev/null
  R=$?
done
echo Docker iniciado com sucesso
echo Iniciando Jenkins agent...
exec su jenkins -c /usr/local/bin/jenkins-agent