export VERSION=4.13-1
export NAME=jenkins-agent
export IMG=registry.gitlab.com/jhmdocker/image/$(NAME)

build:
	docker build --pull -t $(IMG):$(VERSION) --build-arg=VERSION=$(VERSION) .
	docker tag $(IMG):$(VERSION) $(IMG):latest
	docker push $(IMG):$(VERSION)
	docker push $(IMG):latest
