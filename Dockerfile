ARG VERSION=

FROM jenkins/inbound-agent:${VERSION}

USER root

ENV DOCKER_BUILDKIT=1 \
    KUBECTL_VERSION=v1.23.3

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/debian \
        $(lsb_release -cs) \
        stable" && \
    apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io && \
    usermod -a -G docker jenkins

## instala kubectl
RUN curl -sSLlo /usr/local/bin/kubectl "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" && \
    chmod +x /usr/local/bin/kubectl

COPY rootfs /

VOLUME [ "/var/lib/docker" ]

ENTRYPOINT [ "/usr/local/bin/custom-entrypoint.sh" ]
